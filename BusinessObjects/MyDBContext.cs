﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjects
{
    public class MyDBContext : DbContext
    {
        public MyDBContext() { }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            IConfigurationRoot configuration = builder.Build();
            optionsBuilder.UseSqlServer(configuration.GetConnectionString("MyStoreDB"));
        }
        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<Product> Products { get; set; }

        protected override void OnModelCreating(ModelBuilder optionsBuilder)
        {
            optionsBuilder.Entity<Category>().HasData(
                new Category { CategoryId = 1, CategoriesName = "Beverages"},
                new Category { CategoryId = 2, CategoriesName = "Condiments"},
                new Category { CategoryId = 3, CategoriesName = "Confections"},
                new Category { CategoryId = 4, CategoriesName = "Dairy Products"},
                new Category { CategoryId = 5, CategoriesName = "Grains/Cereals"},
                new Category { CategoryId = 6, CategoriesName = "Meat/Poultry"},
                new Category { CategoryId = 7, CategoriesName = "Produce"},
                new Category { CategoryId = 8, CategoriesName = "Seafood"}
            );
        }
    }
}
